# Overview

Create REST APIs to be consumed by a mobile or web app for providing countries detail.  

# Technology Stack

You need to use NodeJS with ExpressJS for creating the API. And use MongoDB as database.

# Terminology and assumptions:

* API will list all the countries
* API will return detail of a country based on its id
* The UI will be built by someone else - you are simply making the REST API endpoints to be
consumed by the front end.
* You will be writing the code as if it’s for production use and should thus have the required
performance and security.

# API Response format

This will be response format for all the APIs, for both success and error

```json
{
    "message" : "Any message need to show to the client",
    "data": {

    }
}
```
- message: This will be any message need to show to the end user 
- data: This will be type of object, and have data returned by the API. If there is no data then this will have an empty object.

And proper error code is required for all the api responses

- 400 Bad Request: For any wrong data submitted through the API
- 401 Unauthorized: For all the unauthorized API calls
- 403 Forbidded: For all the forbidden api calls
- 200 Success: For all success API calls
- 500 Internal Server Error: For all unhandled errors (Avoid this status by handling most of the cases)

# Database

## Country (Table name: countries)

This table will store all the countries

Fields:

- id (int)
- name (string)
- cca (string)
- currency_code (string)
- currency (string)
- capital (string)
- region (string)
- subregion (string)
- area (bigint)
- map_url (url)
- population (bigint)
- flag_url (string)
- created_at (datetime)
- updated_at (datetime)


## CountryNeighbour (Table name: country_neighbours)

This table will store all the neighbours of a country

Fields:

- id (string)
- country_id (string)
- neighbour_country_id (string)
- created_at (string)
- updated_at (string)


Use SQLAlchemy relationship to relate the two tables [https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/#one-to-many-relationships](https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/#one-to-many-relationships)


# APIs required

## 1. API to return all the countries

Curl:

```bash
curl --location 'http://localhost:5001/country'
```

### Use Cases:

- This api will return all the countries in an array with 200 response code

```json
{
    "message": "Country list",
    "data" : {
        "list": [
            {
                "id": 25,
                "name": "India",
                "cca3": "IND",
                "currency_code": "INR",
                "currency": "Indian rupee",
                "capital": "New Delhi",
                "region": "Asia",
                "subregion": "Southern Asia",
                "area": 3287590.0,
                "map_url": "https://goo.gl/maps/WSk3fLwG4vtPQetp7",
                "population": 1380004385,
                "flag_url": "https://flagcdn.com/w320/in.png"
            }
        ]
    }
}
```


## 2. API to get a country detail

Curl:

```bash
curl --location 'http://localhost:5001/country/25'
```

### Use Cases:

- This api will return the country detail of based on the country id passed in the url (ie for above curl the country id is 25)

- Response will contain a single country detail

```json
{
    "message" : "Country detail",
    "data": {
        "country":  {
                "id": 25,
                "name": "India",
                "cca3": "IND",
                "currency_code": "INR",
                "currency": "Indian rupee",
                "capital": "New Delhi",
                "region": "Asia",
                "subregion": "Southern Asia",
                "area": 3287590.0,
                "map_url": "https://goo.gl/maps/WSk3fLwG4vtPQetp7",
                "population": 1380004385,
                "flag_url": "https://flagcdn.com/w320/in.png"
            }
    }
}
```

- If the country is not found in the database using the country id passed in the API url return a JSON reponse with country code 404 (Not found)

```json
{
    "message": "Country not found",
    "data": {}
}
```

## 3. API to get country neighnbours

This api will return the list of countries which are neighbour to the queries county

Curl

```bash
curl --location 'http://localhost:5001/country/25/neighbour'
```

### Use Cases:

- The response is same as country list

```json
{
    "message": "Country neighnbours",
    "data" : {
        "countries": [
            {
                "id": 25,
                "name": "India",
                "cca3": "IND",
                "currency_code": "INR",
                "currency": "Indian rupee",
                "capital": "New Delhi",
                "region": "Asia",
                "subregion": "Southern Asia",
                "area": 3287590.0,
                "map_url": "https://goo.gl/maps/WSk3fLwG4vtPQetp7",
                "population": 1380004385,
                "flag_url": "https://flagcdn.com/w320/in.png"
            }
        ]
    }
}
```

- If the country is not found in the database using the country id passed in the API url return a JSON reponse with country code 404 (Not found)

```json
{
    "message": "Country not found",
    "data": {}
}
```

- If there are no neighbour of the queried country return blank array as response

```json
{
    "message": "Country neighnbours",
    "data" : {
        "list": []
    }
}
```

## 4. API to get countries in sorted order

This API will return the counties based on sorting rules. This is an extension of country list api, request will take an additionla parameter 'sort_by' 

Value of sort by can be:

- a_to_z: Sort the countries in alphabetic order
- z_to_a: Sort the countries in reverse alphabetic order
- population_high_to_low: Sort the countries based on polulation in decreasing order
- population_low_to_high: Sort the countries based on population in ascending order
- area_high_to_low: Sort the countries based on area in decreasing order
- area_low_to_high: Sort the countries based on area in ascending order

If sort_by query parameter is missing sort the list by 'a_to_z'

Curl:

```bash
curl --location 'http://localhost:5001/country?sort_by=a_toz'
```

User cases:

- Response will look same as country list API

## 5. API to get countries in paginated list

This API will list all the countries in paginated list. This is an extension of existing country list API.

```bash
curl --location 'http://localhost:5001/country?sort_by=a_toz&page=1&limit=10'
```

Request Query Parameter:

- Page (int): Return the country list based on this page number. If page is not specified in API, use 1 as default 

```
If page = 1, sort the countries based on sort_by query parameter and return recent 10 (based on limit query parameter) countries. 
```

```
If page = 3, sort the countries based on sort_by query parameter and return recent 10 (based on limit query parameter) countries, skipping 2 pages, that is countries from 21 to 30
```

- Limit (int): Return the number of records per page.
If limit is not specified in the API, use 10 as default limit

### Use Cases:

- The country list will be in paginated form, all the APIs will return few paginated data, please find below the sample response

```json
{
    "message": "Country list",
    "data": {
        "list": [],
        "has_next": true,
        "has_prev": false,
        "page": 1,
        "pages": 10,
        "per_page": 10,
        "total": 98
    }
}
```

- And refer to [Flask SqlAlchemy Paginate](https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/api/#flask_sqlalchemy.SQLAlchemy.paginate) for pagination
- If there are no countries, return 200 success response with no data in the 'list'

6. API to search a countries by:
    * Name
    * Region
    * Subregion

This API is an extension of previous API with few more new query parameter name, region and subregion. Based on the values of these parameters the list will be filtered

Parameters:

- name: If name is passed the country list will be filtered by the name
- region: If region is passed the country list will be filtered by the region
- subregion: If subregion is passed the country list will be filtered by the subregion

Curl:

```bash
curl --location 'http://localhost:5001/country?sort_by=a_toz&page=1&limit=10&name=united&region=asia&subregion='
```

Response will be same a paginated country list API

# Data Source

Similar API is developed and deployed at this URL. You can take the reference. And you can use this website APIs to feed your database for better results: [https://restcountries.com/#api-endpoints-v3](https://restcountries.com/#api-endpoints-v3)

# Evaluation criteria:
* Completeness of functionality
* Correctness under thorough testing
* Performance and scalability of APIs
* Security of APIs
* Data modeling
* Structure of code
* Readability of code

# Deployment:
* Deploy the APIs on any of the public URL

# Assignment Submition:
* Push the assignment on Gitlab or Github and share the URL as submission
* Share the postman collection along with the Git URL
* Share the public URL of the application
